#include "Fenetre.h"
#include "FenetreGraphique.h"
#include "Interpreteur.h"
#include <iostream>

Fenetre::Fenetre()
{
    //Barre d'outils
    menuFichier = menuBar()->addMenu("&Fichier");
    menuEdition = menuBar()->addMenu("&Editeur");
    menuAide = menuBar()->addMenu("&Aide");

    actionOuvrirFichier = new QAction("&Ouvrir un fichier", this);
    actionOuvrirFichier->setShortcut(QKeySequence("Ctrl+O"));

    actionQuitter = new QAction("&Quitter", this);
    actionQuitter->setShortcut(QKeySequence("Ctrl+Q"));

    actionNouveauFichier = new QAction("&Nouveau Document", this);
    actionNouveauFichier->setShortcut(QKeySequence("Ctrl+N"));

    actionSauverFichier = new QAction("&Sauvegarder le fichier", this);
    actionSauverFichier->setShortcut(QKeySequence("Ctrl+S"));

    actionBitBucket = new QAction("&Accéder à la page BitBucket", this);
    actionDoc = new QAction("Documentation", this);

    //Ajout des actions dans la barre d'outils
    menuFichier->addAction(actionOuvrirFichier);
    menuFichier->addAction(actionQuitter);
    menuEdition->addAction(actionNouveauFichier);
    menuEdition->addAction(actionSauverFichier);
    menuAide->addAction(actionBitBucket);
    menuAide->addAction(actionDoc);


    //Console
    console = new QTextEdit;
    console->setReadOnly(true);
    console->setStyleSheet("background: black; color: white;");

    //Champs de ligne de commande
    commande = new QLineEdit;

    //Envoyer la commande
    envoyer     = new QPushButton("Envoyer");

    //Mise en forme du champs et du bouton
    layoutCommande = new QHBoxLayout;
    layoutCommande->addWidget(commande);
    layoutCommande->addWidget(envoyer);

    //Mise en fome générale de la console
    layoutConsole = new QVBoxLayout;
    layoutConsole->addWidget(console);
    layoutConsole->addLayout(layoutCommande);

    //Editeur de texte
    editeur = new QTextEdit;
    editeur->setEnabled(false);

    //Initialisation des widgets
    nouveau = new QPushButton("Nouveau Fichier");

    sauvegarder = new QPushButton("Sauvegarder");
    sauvegarder->setEnabled(false);

    btnEnregistrerSous = new QPushButton("Enregistrer Sous");
    btnEnregistrerSous->setEnabled(false);

    executer = new QPushButton("Exécuter");
    executer->setEnabled(false);

    cheminFichier = new QLabel("Aucun fichier ouvert");
    etatFichier = new QLabel("Aucun fichier ouvert");

    //Boutons de l'éditeur
    boutonsEditeur = new QHBoxLayout;
    boutonsEditeur->addWidget(nouveau);
    boutonsEditeur->addWidget(executer);
    boutonsEditeur->addWidget(btnEnregistrerSous);
    boutonsEditeur->addWidget(sauvegarder);

    //Valeurs de l'éditeur (Hauteur, Largeur)
    labelHauteur = new QLabel("Hauteur :");
    labelLargeur = new QLabel("Largeur :");

    valeurHauteur = new QLineEdit;
    valeurLargeur = new QLineEdit;

    valeurHauteur->setEnabled(false);
    valeurLargeur->setEnabled(false);

    layoutValeurs = new QHBoxLayout;
    layoutValeurs->addWidget(labelHauteur);
    layoutValeurs->addWidget(valeurHauteur);
    layoutValeurs->addWidget(labelLargeur);
    layoutValeurs->addWidget(valeurLargeur);

    //Mise en forme de l'éditeur
    layoutEditeur = new QVBoxLayout;
    layoutEditeur->addLayout(boutonsEditeur);
    layoutEditeur->addLayout(layoutValeurs);
    layoutEditeur->addWidget(cheminFichier);
    layoutEditeur->addWidget(editeur);
    layoutEditeur->addWidget(etatFichier);

    //Calculatrice
    executerCalculatrice = new QPushButton("Executer");

    connect(executerCalculatrice, SIGNAL(clicked()), this, SLOT(execCalculatrice()));

    touches = new QGridLayout;

    //Création des touches numerotées et leurs signaux
    for (int i = 0; i < 10; i++)
    {
        QString text = QString::number(i);
        touche[i] = new QPushButton(text, this);
        connect(touche[i], SIGNAL(clicked()), this, SLOT(clickTouche()));
    }

    //Création des touche "fonctions" de la calculatrice
    effacer = new QPushButton("Effacer");
    virgule = new QPushButton(".");
    plus = new QPushButton("+");
    moins = new QPushButton("-");
    fois = new QPushButton("*");
    diviser = new QPushButton("/");
    push = new QPushButton("push");
    pushstr = new QPushButton("pushstr");
    copy = new QPushButton("copy");
    copystr = new QPushButton("copystr");
    print = new QPushButton("print");
    printstr = new QPushButton("printstr");
    display = new QPushButton("display");
    displaystr = new QPushButton("displaystr");
    count = new QPushButton("count");
    countstr = new QPushButton("countstr");
    swap = new QPushButton("swap");
    swapstr = new QPushButton("swapstr");
    sin = new QPushButton("sin");
    cos = new QPushButton("cos");
    tan = new QPushButton("tan");
    sqrt = new QPushButton("sqrt");
    trunc = new QPushButton("trunc");
    line = new QPushButton("line");
    color = new QPushButton("color");
    drawstr = new QPushButton("drawstr");

    //Création des signaux de toutes les touches de la calculatrice
    connect(effacer, SIGNAL(clicked()), this, SLOT(clickEffacer()));

    connect(plus, SIGNAL(clicked()), this, SLOT(clickFonction()));
    connect(moins, SIGNAL(clicked()), this, SLOT(clickFonction()));
    connect(fois, SIGNAL(clicked()), this, SLOT(clickFonction()));
    connect(diviser, SIGNAL(clicked()), this, SLOT(clickFonction()));
    connect(push, SIGNAL(clicked()), this, SLOT(clickFonction()));
    connect(pushstr, SIGNAL(clicked()), this, SLOT(clickFonction()));
    connect(copy, SIGNAL(clicked()), this, SLOT(clickFonction()));
    connect(copystr, SIGNAL(clicked()), this, SLOT(clickFonction()));
    connect(print, SIGNAL(clicked()), this, SLOT(clickFonction()));
    connect(printstr, SIGNAL(clicked()), this, SLOT(clickFonction()));
    connect(display, SIGNAL(clicked()), this, SLOT(clickFonction()));
    connect(displaystr, SIGNAL(clicked()), this, SLOT(clickFonction()));
    connect(count, SIGNAL(clicked()), this, SLOT(clickFonction()));
    connect(countstr, SIGNAL(clicked()), this, SLOT(clickFonction()));
    connect(swap, SIGNAL(clicked()), this, SLOT(clickFonction()));
    connect(swapstr, SIGNAL(clicked()), this, SLOT(clickFonction()));
    connect(sin, SIGNAL(clicked()), this, SLOT(clickFonction()));
    connect(cos, SIGNAL(clicked()), this, SLOT(clickFonction()));
    connect(tan, SIGNAL(clicked()), this, SLOT(clickFonction()));
    connect(sqrt, SIGNAL(clicked()), this, SLOT(clickFonction()));
    connect(trunc, SIGNAL(clicked()), this, SLOT(clickFonction()));
    connect(line, SIGNAL(clicked()), this, SLOT(clickFonction()));
    connect(color, SIGNAL(clicked()), this, SLOT(clickFonction()));
    connect(drawstr, SIGNAL(clicked()), this, SLOT(clickFonction()));


    connect(virgule, SIGNAL(clicked()), this, SLOT(clickTouche()));

    //Placement des touches de la calculatrice
    for (int i = 0; i < 9; ++i)
        touches->addWidget(touche[i + 1], i / 3, i % 3);

    //Placement des touches de la calculatrice
    touches->addWidget(touche[0], 3, 1);
    touches->addWidget(effacer, 6, 0);
    touches->addWidget(virgule, 3, 0);
    touches->addWidget(plus, 0, 3);
    touches->addWidget(moins, 0, 4);
    touches->addWidget(fois, 1, 3);
    touches->addWidget(diviser, 1, 4);
    touches->addWidget(push, 2, 3);
    touches->addWidget(pushstr, 2, 4);
    touches->addWidget(copy, 3, 3);
    touches->addWidget(copystr, 3, 4);
    touches->addWidget(print, 4, 3);
    touches->addWidget(printstr, 4, 4);
    touches->addWidget(display, 5, 3);
    touches->addWidget(displaystr, 5, 4);
    touches->addWidget(count, 6, 3);
    touches->addWidget(countstr, 6, 4);
    touches->addWidget(swap, 7, 3);
    touches->addWidget(swapstr, 7, 4);
    touches->addWidget(sin, 0, 5);
    touches->addWidget(cos, 1, 5);
    touches->addWidget(tan, 2, 5);
    touches->addWidget(sqrt, 3, 5);
    touches->addWidget(trunc, 4, 5);
    touches->addWidget(line, 5, 5);
    touches->addWidget(color, 6, 5);
    touches->addWidget(drawstr, 7, 5);

    //Layout de la calculatrice
    resultat = new QTextEdit;
    resultat->setReadOnly(true);
    layoutCalculatrice = new QVBoxLayout;
    layoutCalculatrice->addWidget(executerCalculatrice);
    layoutCalculatrice->addWidget(resultat);
    layoutCalculatrice->addLayout(touches);

    //Création des onglets
    onglets = new QTabWidget;

    pageConsole = new QWidget;
    pageEditeur = new QWidget;
    pageCalculatrice = new QWidget;

    pageConsole->setLayout(layoutConsole);
    pageEditeur->setLayout(layoutEditeur);
    pageCalculatrice->setLayout(layoutCalculatrice);

    //Ajout des onglets
    onglets->addTab(pageConsole, "Console");
    onglets->addTab(pageEditeur, "Editeur");
    onglets->addTab(pageCalculatrice, "Calculatrice");

    //Arborescence de fichiers

    //Model de l'arborescence
    model = new QDirModel(this);
    model->setReadOnly(true);//Empêcher la modification
    model->setSorting(QDir::DirsFirst | QDir::IgnoreCase);//Triage

    //Création de l'arborescence
    fichiers = new QTreeView;
    fichiers->setModel(model);
    fichiers->hideColumn(1);
    fichiers->hideColumn(2);
    fichiers->hideColumn(3);

    ouvrir = new QPushButton("Ouvrir");

    //Layout de l'arborescence
    layoutFichiers = new QVBoxLayout;
    layoutFichiers->addWidget(fichiers);
    layoutFichiers->addWidget(ouvrir);

    //Layout général de l'application
    layoutGeneral = new QHBoxLayout;
    layoutGeneral->addWidget(onglets);
    layoutGeneral->addLayout(layoutFichiers);
    layoutGeneral->setStretchFactor(onglets, 4);
    layoutGeneral->setStretchFactor(layoutFichiers, 1);

    //Definition de la zone centrale
    zoneCentrale = new QWidget;
    zoneCentrale->setLayout(layoutGeneral);

    setCentralWidget(zoneCentrale);
    setWindowTitle("Interpreteur NPI");

    commande->setFocus(); //Focus sur le champs de commande

    //SLOTS

    //Liste de fichiers
    connect(ouvrir, SIGNAL(clicked()), this, SLOT(parcourirFichiers()));
    connect(fichiers, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(ouvrirFichier()));

    //Envoyer une commande
    connect(envoyer, SIGNAL(clicked()), this, SLOT(envoyerCmd()));
    connect(commande, SIGNAL(returnPressed()), this, SLOT(envoyerCmd()));

    //Barre d'outils
    connect(actionOuvrirFichier, SIGNAL(triggered()), this, SLOT(parcourirFichiers()));
    connect(actionQuitter, SIGNAL(triggered()), qApp, SLOT(quit()));
    connect(actionBitBucket, SIGNAL(triggered()), this, SLOT(ouvrirBitBucket()));
    connect(actionDoc, SIGNAL(triggered()), this, SLOT(ouvrirDoc()));
    connect(sauvegarder, SIGNAL(clicked()), this, SLOT(sauverFichier()));

    //Editeur
    connect(nouveau, SIGNAL(clicked()), this, SLOT(nouveauFichier()));
    connect(actionNouveauFichier, SIGNAL(triggered()), this, SLOT(nouveauFichier()));
    connect(btnEnregistrerSous, SIGNAL(clicked()), this, SLOT(enregistrerSous()));
    connect(sauvegarder, SIGNAL(clicked()), this, SLOT(sauverFichier()));
    connect(actionSauverFichier, SIGNAL(triggered()), this, SLOT(sauverFichier()));
    connect(editeur, SIGNAL(textChanged()), this, SLOT(modifierEtatFichier()));
    connect(executer, SIGNAL(clicked()), this, SLOT(executerProgramme()));
}


/*
 * Gestion des fichiers
 */

//Ouverture d'un fichier et initialisation de l'éditeur
void Fenetre::ouvrirFichier()
{
    QString chemin = model->filePath(fichiers->currentIndex()); //Chemin du fichier
    QString ext = model->fileInfo(fichiers->currentIndex()).suffix(); //Récupération de l'extension du fichier

    //Ouverture du fichier
    if(ext.toUpper() == "NPI")
    {
        nouveauFichier();
        console->append("Ouverture du fichier : " + chemin);
        editerFichier(chemin);
        cheminFichier->setText(chemin);
        etatFichier->setText("Sauvegardé");
    }
    else
        console->append("Erreur : Mauvaise extension ! (Seul un fichier .NPI peut être ouvert)");
}

//Ouverture de la fenêtre permettant de selectionner le fichier à ouvrir
void Fenetre::parcourirFichiers()
{
    //Affiche une fenetre permettant de choisir un fichier .npi
    QString chemin = QFileDialog::getOpenFileName(this, "Ouvrir un fichier", QString(), "Fichier NPI(*.npi)");

    //Ouverture du fichier
    if(!chemin.isEmpty())
    {
        nouveauFichier();
        console->append("Ouverture du fichier : " + chemin);
        editerFichier(chemin);
        cheminFichier->setText(chemin);
        etatFichier->setText("Sauvegardé");
    }
}


/*
 * Console
 */

//Affichage d'une commande dans la console
void Fenetre::envoyerCmd()
{
    QString cmd = commande->text();
    console->append(">: " + cmd);
    commande->clear();
    commande->setFocus();
}


/*
 * Editeur
 */

//Création un fichier dans l'éditeur
void Fenetre::nouveauFichier()
{
    //Initialisation des indicateurs
    cheminFichier->setText("Nouveau");
    etatFichier->setText("Non-sauvegardé");

    //Affiche l'onglet "console"
    onglets->setCurrentIndex(1);

    //Active les différents widgets de l'éditeur
    editeur->setEnabled(true);
    btnEnregistrerSous->setEnabled(true);
    sauvegarder->setEnabled(true);
    executer->setEnabled(true);

    valeurHauteur->setEnabled(true);
    valeurLargeur->setEnabled(true);
}

//Ouvre une fenêtre pour choisir un chemin d'enregistrement
void Fenetre::enregistrerSous()
{
    //Ouvre une fenêtre permettant de choisir le chemin de l'enregistrement
    QString chemin = QFileDialog::getSaveFileName(this, "Enregistrer un fichier", QString(), "Fichier NPI(*.npi)");
    cheminFichier->setText(chemin);
    sauverFichier();
}

//Enregistrement d'un fichier
void Fenetre::sauverFichier()
{
    //Récupération du chemin du fichier
    QString chemin = cheminFichier->text();

    //Si c'est un nouveau fichier on demande un chemin pour l'enregistrer
    if(chemin == "Nouveau")
    {
        enregistrerSous();
        chemin = cheminFichier->text();
    }

    QFile fichier(chemin);
    fichier.remove(); //Suppression d'un éventuel fichier déjà présent

    //Ouverture du fichier et affichage dans l'éditeur
    fichier.open(QIODevice::Append);
    QTextStream out(&fichier);
    out << editeur->toPlainText() << endl;

    etatFichier->setText("Sauvegardé");
}

//Ouvre un fichier dans l'éditeur
void Fenetre::editerFichier(QString chemin)
{
    //Vide l'éditeur
    editeur->clear();

    //Ouverture du fichier
    QFile fichier(chemin);
    if (!fichier.open(QIODevice::ReadOnly | QIODevice::Text))
        return;

    //Affiche le contenu du fichier dans l'éditeur
    QTextStream in(&fichier);
    QString ligne;
    while (!in.atEnd())
    {
        ligne = in.readLine();
        editeur->append(ligne);
    }
}

//Affcher si le fichier est sauvegardé ou non
void Fenetre::modifierEtatFichier()
{
    etatFichier->setText("Non-sauvegardé");
}

//Lecture du fichier et exécution des commandes
void Fenetre::executerProgramme()
{
    sauverFichier();

    QString chemin = cheminFichier->text(); //Récupération du chemin du fichier

    //Définition de la largeur de la fenêtre affichant du graphique
    int largeur = 500;
    int hauteur = 500;

    //Si des valeurs personnalisés sont entrées on les ajoutés
    if(valeurLargeur->text().toInt() != 0)
        largeur = valeurLargeur->text().toInt();

    if(valeurHauteur->text().toInt() != 0)
        hauteur = valeurHauteur->text().toInt();

    //Création de la fenêtre graphique
    FenetreGraphique *graphiques = new FenetreGraphique(largeur, hauteur);
    graphiques->show();

    //Création de l'interpréteur et lecture des commandes
    Interpreteur interpreteur(console, graphiques, valeurLargeur->text().toInt(), valeurHauteur->text().toInt());
    interpreteur.lireFichier(chemin);

    onglets->setCurrentIndex(0); //Affiche l'onglet "console"
}


/*
 * Calculatrice
 */

//Appuie sur une touche de la calculatrice
void Fenetre::clickTouche()
{
    QPushButton *touche = (QPushButton *)sender();
    resultat->insertPlainText( touche->text() );
}

//Appuie sur une touche fonction de la calculatrice
void Fenetre::clickFonction()
{
    QPushButton *fonction = (QPushButton *)sender();
    resultat->insertPlainText( " " + fonction->text() + " " );
}

//Exécute les commandes entrées dans la calculatrice
void Fenetre::execCalculatrice()
{
    //Création d'un fichier temporaire pour stocker les commandes
    QString chemin("temp.npi");

    QFile fichier(chemin);
    fichier.remove(); //Suppression d'un éventuel fichier déjà présent

    //Ajoute les commandes de la calculatrice dans le fichier temporaire
    fichier.open(QIODevice::Append);
    QTextStream out(&fichier);
    out << resultat->toPlainText() << endl;

    //Définition de la largeur de la fenêtre affichant du graphique
    int largeur = 500;
    int hauteur = 500;

    //Si des valeurs personnalisés sont entrées on les ajoutés
    if(valeurLargeur->text().toInt() != 0)
        largeur = valeurLargeur->text().toInt();

    if(valeurHauteur->text().toInt() != 0)
        hauteur = valeurHauteur->text().toInt();

    //Création de la fenêtre graphique
    FenetreGraphique *graphiques = new FenetreGraphique(largeur, hauteur);
    graphiques->show();

    //Création de l'interpréteur et lecture des commandes
    Interpreteur interpreteur(console, graphiques, valeurLargeur->text().toInt(), valeurHauteur->text().toInt());
    interpreteur.lireFichier(chemin);

    onglets->setCurrentIndex(0); //Affiche l'onglet "console"
}

//Efface le contenue de la calculatrice
void Fenetre::clickEffacer()
{
    resultat->clear();
}


/*
 * Aide
 */

//Ouvre le navigateur vers la page BitBucket
void Fenetre::ouvrirBitBucket()
{
    QDesktopServices::openUrl(QUrl("https://bitbucket.org/whiteflagteam/calculatrice-n.p.i"));
}

//Ouvre le navigateur vers la documentation
void Fenetre::ouvrirDoc()
{
    QDesktopServices::openUrl(QUrl("http://whiteflagteam.com/npi"));
}
