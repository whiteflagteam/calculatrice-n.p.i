#ifndef FENETRE_H
#define FENETRE_H

#include <QtWidgets>
#include <QTreeView>
#include <QDirModel>

class Fenetre: public QMainWindow
{
    Q_OBJECT

    public:
        Fenetre();

    private slots:
        //Gestion des fichiers
        /**
         * @brief Ouvre un fichier
         */
        void ouvrirFichier();

        /**
         * @brief Parcours les fichiers
         */
        void parcourirFichiers();

        //Console
        /**
         * @brief Affiche la commande sur la console
         */
        void envoyerCmd();

        //Editeur
        /**
         * @brief Crée un nouveau fichier dans l'éditeur
         */
        void nouveauFichier();
        /**
         * @brief Enregistre le fichier dans un emplacement choisie
         */
        void enregistrerSous();

        /**
         * @brief Sauvegarde le fichier courant
         */
        void sauverFichier();

        /**
         * @brief Edite le fichier
         * @param chemin QString du chemin du fichier
         */
        void editerFichier(QString chemin);

        /**
         * @brief Modifier l'état du fichier(sauvegarder ou non)
         */
        void modifierEtatFichier();

        /**
         * @brief Execute le script npi
         */
        void executerProgramme();

        //Calculatrice
        void clickTouche();
        void clickFonction();
        void execCalculatrice();
        void clickEffacer();

        //Aide
        void ouvrirBitBucket();
        void ouvrirDoc();

    private:
        //Barre d'outils
        QMenu *menuFichier;
        QMenu *menuEdition;
        QMenu *menuAide;
        QAction *actionOuvrirFichier;
        QAction *actionQuitter;
        QAction *actionNouveauFichier;
        QAction *actionSauverFichier;
        QAction *actionBitBucket;
        QAction *actionDoc;


        //Console
        QTextEdit   *console;
        QLineEdit   *commande;
        QPushButton *envoyer;
        QHBoxLayout *layoutCommande;
        QVBoxLayout *layoutConsole;

        //Editeur
        QTextEdit *editeur;
        QPushButton *nouveau;
        QPushButton *sauvegarder;
        QPushButton *btnEnregistrerSous;
        QPushButton *executer;
        QLabel *cheminFichier;
        QLabel *etatFichier;

        //Valeur de la taille de la fenetre des graphiques
        QLabel *labelHauteur;
        QLabel *labelLargeur;
        QLineEdit *valeurHauteur;
        QLineEdit *valeurLargeur;

        //Layouts Editeur
        QHBoxLayout *layoutValeurs;
        QHBoxLayout *boutonsEditeur;
        QVBoxLayout *layoutEditeur;

        //Calculatrice
        QTextEdit *resultat;

        //Touche des la calculatrice
        QPushButton *executerCalculatrice;
        QPushButton *touche[10];
        QPushButton *effacer;
        QPushButton *virgule;
        QPushButton *plus;
        QPushButton *moins;
        QPushButton *fois;
        QPushButton *diviser;
        QPushButton *push;
        QPushButton *pushstr;
        QPushButton *copy;
        QPushButton *copystr;
        QPushButton *print;
        QPushButton *printstr;
        QPushButton *display;
        QPushButton *displaystr;
        QPushButton *count;
        QPushButton *countstr;
        QPushButton *swap;
        QPushButton *swapstr;
        QPushButton *sin;
        QPushButton *cos;
        QPushButton *tan;
        QPushButton *sqrt;
        QPushButton *trunc;
        QPushButton *line;
        QPushButton *color;
        QPushButton *drawstr;
        QGridLayout *touches;

        QVBoxLayout *layoutCalculatrice;

        //Onglets
        QTabWidget *onglets;
        QWidget *pageConsole;
        QWidget *pageEditeur;
        QWidget *pageCalculatrice;
        QWidget *graphique;

        //Arborescence de fichiers
        QDirModel   *model;
        QTreeView   *fichiers;
        QPushButton *ouvrir;
        QVBoxLayout *layoutFichiers;
        QHBoxLayout *layoutGeneral;

        //Zone centrale
        QWidget *zoneCentrale;


};

#endif // FENETRE_H
