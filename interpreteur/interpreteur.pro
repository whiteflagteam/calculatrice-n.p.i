QT += widgets

HEADERS += \
    Fenetre.h \
    FenetreGraphique.h \
    Interpreteur.h

SOURCES += \
    main.cpp \
    Fenetre.cpp \
    FenetreGraphique.cpp \
    Interpreteur.cpp
