#include <QApplication>
#include <QtWidgets>
#include "Fenetre.h"

int main(int argc, char *argv[])
{

    /*! \mainpage My Personal Index Page
     *
     * \section intro_sec Introduction
     *
     * This is the introduction.
     *
     * \section install_sec Installation
     *
     * \subsection step1 Step 1: Opening the box
     *
     * etc...
     */

    QApplication app(argc, argv);

    Fenetre fenetre;
    fenetre.setWindowState(fenetre.windowState() ^ Qt::WindowMaximized);//Affichage en plein écran
    fenetre.show();

    return app.exec();
}
