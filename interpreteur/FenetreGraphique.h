#ifndef FENETREGRAPHIQUE_H
#define FENETREGRAPHIQUE_H

#include <QtWidgets>

class FenetreGraphique: public QWidget
{
    Q_OBJECT

    public:
        /**
         * @brief Constructeur de FenetreGraphique
         * @param largeur Largeur de la fenêtre
         * @param hauteur Hauteur de la fenêtre
         */
        FenetreGraphique(int largeur = 500, int hauteur = 500);

        /**
         * @brief Ajoute une ligne a la fenêtre graphique à partir des coordonées de 2 points
         * @param x1 Coordonée x du premier point
         * @param y1 Coordonée y du premier point
         * @param x2 Coordonée x du deuxième point
         * @param y2 Coordonée x du deuxième point
         */
        void addLine(int x1,int y1,int x2,int y2);

        /**
         * @brief Definie la couleur du tracé
         * @param couleur Index de la couleur
         */
        void setColor(int couleur);

        /**
         * @brief addText Ajoute un texte à la fenêtre graphique
         * @param x Coordonée x du texte
         * @param y Coordonée y du texte
         * @param texte Texte à afficher
         */
        void addText(int x, int y, QString texte);

    protected:
        void paintEvent(QPaintEvent *event);

    private:
        QColor *color;
        QList<QLine> listeLine;
        QList<QPoint> listeQPoint;
        QList<QString> listeTexte;
};

#endif // FENETREGRAPHIQUE_H
