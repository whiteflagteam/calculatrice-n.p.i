#include "FenetreGraphique.h"

FenetreGraphique::FenetreGraphique(int largeur, int hauteur)
{
    resize(largeur,hauteur);
    setWindowTitle("Interpreteur - NPI | Graphique");
    color = new QColor;
}

void FenetreGraphique::paintEvent(QPaintEvent *)
{
    QPainter painter(this);

    //painter.setBrush(Qt::GlobalColor);
    painter.setPen((*color));

    //trace les lignes si il toutes les coordonées sont renseignées
    for(int i = 0; i < listeLine.count(); i++)
    {
        painter.drawLine(listeLine[i]);
    }

    for(int j = 0; j < listeQPoint.count(); j++)
    {
        painter.drawText(listeQPoint[j], listeTexte[j]);
    }

    painter.save();
    painter.restore();

    update();
}

//Ajout d'une ligne dans la fenêtre graphique
void FenetreGraphique::addLine(int x1, int y1, int x2, int y2)
{
    QLine ligne(x1,y1,x2,y2);

    listeLine << ligne;
}

//Sélection d'une couleur
void FenetreGraphique::setColor(int couleur)
{
    switch(couleur)
    {
        case 0:
            color->setNamedColor(QString("black"));
            break;
        case 1:
            color->setNamedColor(QString("white"));
            break;
        case 2:
            color->setNamedColor(QString("red"));
            break;
        case 3:
            color->setNamedColor(QString("green"));
            break;
        case 4:
            color->setNamedColor(QString("blue"));
            break;
        case 5:
            color->setNamedColor(QString("cyan"));
            break;
        case 6:
            color->setNamedColor(QString("magenta"));
            break;
        case 7:
            color->setNamedColor(QString("yellow"));
            break;
    }
}

void FenetreGraphique::addText(int x, int y, QString texte)
{
    QPoint point;
    point.setX(x);
    point.setY(y);

    listeQPoint << point;
    listeTexte << texte;
}
