#ifndef INTERPRETEUR_H
#define INTERPRETEUR_H

#include <QtWidgets>

#include <iostream>
#include <string>
#include <typeinfo>
#include <fstream>
#include <vector>
#include <cmath>
#include <map>

using namespace std;

#include "Fenetre.h"
#include "FenetreGraphique.h"

class Interpreteur: public QWidget
{
    Q_OBJECT

    public:
       /**
         * @brief Constructeur de interpreteur
         * @param console Pointeur sur la console graphique
         * @param graphiques Pointeur sur la fenêtre graphique
         */
        Interpreteur(QTextEdit *console, FenetreGraphique *graphiques, int, int);

        /**
         * @brief Permet de lire le fichier
         * @param chemin Chemin du fichier à ouvrir
         */
        void lireFichier(QString chemin);

        QTextEdit *console;
        FenetreGraphique *graphiques;

    private:
        int largeur;
        int hauteur;
        Fenetre fenetre;

};

#endif // INTERPRETEUR_H
