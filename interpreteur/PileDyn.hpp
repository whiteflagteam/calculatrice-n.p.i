#ifndef PILE_HPP
#define PILE_HPP

#define PI 3.14159265


template<typename T>
class Pile {
	private :
		vector<T> pile;
		QTextEdit *console;
		T input;
		QTextStream *flux;
		FenetreGraphique *graphiques;
        Pile<double> *pilenum;


	public :
        /**
         * @brief Constructeur de pile
         * @param _console Pointer sur la console Qt
         * @param _flux Pointer sur flux du fichier npi lu
         * @param _graphiques Pointer sur la fenêtre graphique
         * @param _pilenum Pointer sur la la pile numérique si la pile est une pile de string
         */
        Pile(QTextEdit *_console, QTextStream *_flux, FenetreGraphique *_graphiques, Pile<double> *_pilenum = 0) : console(_console), flux(_flux), graphiques(_graphiques), pilenum(_pilenum ) {}


        /**
          * DEBUT commandes sur la pile numérique
          */

        /**
         * @brief Permet d'afficher la pile numérique
         * @return QString du contenue de la pile numérique
         */
        QString show() const {
        	QString result;
        	result = "[";
			for (unsigned int i=0; i< pile.size(); i++){
				result += QString::number(pile[i]) + " ";
			}
			result += "]";
			return result;
        }

        /**
         * @brief Empile un élément lorsque la fonction push du fichier npi est appelé
         */
        void push() {
				(*flux) >> input;
                pile.push_back(input);
		}

        /**
         * @brief Empile un élément
         * @param element
         */
		void push(T element) {
			pile.push_back(element);
		}

        /**
         * @brief Dépile un élement de la pile
         * @return Elément dépilé
         */
		T depiler() {
			if (!estVide()) {
                T last = pile.back();
                pile.pop_back();
				return last;
			}
			else {
				throw "C'est vide ";
			}
		}

        /**
         * @brief Executé depiler(pour la map)
         */
		void pop() {
			depiler();
		}


        /**
         * @brief Inverse les deux derniers éléments de la pile
         */
		void swap(){
		    T a = pile.back(); // R�cup�re le dernier �l�ment
		    pile[pile.size()-1]= pile[pile.size()-2]; // On inverse le dernier �lement avec l'avant-dernier �l�ment
		    pile[pile.size()-2] = a;
        }

        /**
         * @brief Copie le ieme élement de la pile numérique en fonction du dernier élément
         */
        void copy() {
            T a = depiler();
            if (a > 0) {
                int b = (int) a;
                b = (pile.size()-b)-1;
                //cout << typeid(a).name() << endl;
                //cout << a << endl;
                T valcopier = pile[b];
                push(valcopier);
            } else {
                throw "Valeur incorrect";
            }
        }

        /**
         * @brief Copie le ieme élement de la pile de string en fonction du dernier élément de la pile numérique
         * @param i dernier élément de la pile numérique
         */
        void copy(double i) { //fonction de copie sur une pile de cha�ne de caract�re
            int a = (int) i;
            if (a >= 0) {
                a = (pile.size()-a)-1;
                T valcopier = pile[a];
                push(valcopier);
            } else {
                throw "Valeur incorrect";
            }
        }

        /**
         * @brief Compte le nombre d'élément dans la pile
         * @return La taille de la pille
         */
		unsigned int compter() const{
			return pile.size();
		}

        /**
         * @brief Verifie si la pile est vide
         * @return Booléan
         */
		bool estVide() const
		{
			return (pile.empty());
		}

        /**
         * @brief Affiche l'élément dépilé
         */
		void print() {
            console->append(QString::number(depiler()));
        }

        /**
         * @brief Change le signe du dernier élément
         */
        void signe() {
            T element = depiler();
            element *= -1;
            push(element);
        }

        /**
         * @brief Inverse le dernier élement
         */
		void inverse() {
            T element = depiler();
            element = 1/element;
            push(element);
        }

        /**
         * @brief Additione les deux derniers éléments
         */
        void plus() {
            T a;
            a = depiler();
            a += depiler();
            push(a);
        }

        /**
         * @brief Soustrait les deux derniers éléments
         */
        void minus() {
            T a;
            a = depiler();
            a = depiler()-a;
            push(a);
        }

        /**
         * @brief Multiplie les deux derniers éléments
         */
        void multiply() {
            T a;
            a = depiler();
            a *= depiler();
            push(a);
        }

        /**
         * @brief Divise les deux derniers éléments
         */
        void division() {
            T a;
            a = depiler();
            a /= depiler();
            push(a);
        }

        /**
         * @brief Affiche la pile numérique dans la console
         */
        void display() {
            console->append("Pile Double:" + show());
        }

        /**
         * @brief Empile le nombre d'élément
         */
        void count() {
            push(compter());
        }

        /**
         * @brief Empile le sinus du dernier élément
         */
        void sin() {
            T a = depiler();
            a = std::sin(a*PI/180);
            push(a);
        }

        /**
         * @brief Empile le cosinus du dernier élément
         */
        void cos() {
            T a = depiler();
            a = std::cos(a*PI/180);
            push(a);
        }

        /**
         * @brief Empile la tangente du dernier élément
         */
        void tan() {
            T a = depiler();
            a = std::tan(a*PI/180);
            push(a);
        }

        /**
         * @brief Empile la racine carré du dernier élément
         */
        void sqrt() {
            T a = depiler();
            push(std::sqrt(a));
        }

        /**
         * @brief Arrondie le dernier élément
         */
        void trunc() {
            T a = depiler();
            push(round(a));
        }

        /**
         * @brief Dessine une ligne dans l'interface graphique
         */
        void line() {
            int x1,y1,x2,y2;
            y2 = depiler();
            x2 = depiler();
            y1 = depiler();
            x1 = depiler();

            graphiques->addLine(x1,y1,x2,y2);

        }

        /**
         * @brief Change la couleur des formes géométriques
         */
        void color() {
            int couleur;
            couleur = depiler();

            graphiques->setColor(couleur);
        }

        /**
         * FIN commandes sur la pile numérique
         */


        /**
         * @brief Saute la ligne lorsque qu'un commentaire est rencontré
         */
        void comment() {
            flux->readLine();
        }

        /**
         * DEBUT commandes sur la pile de string
         */

        /**
         * @brief Empile un élément lorsque la fonction pushstr du fichier npi est appelé
         */
        void pushstr() {

            (*flux) >> input;

            if ((input[0] == '"') && (input[input.size()-1] == '"')) {
                    input.remove(0,1);
                    input.remove(input.size()-1,input.size());
                    pile.push_back(input);
            } else {
                console->append(QString("Syntaxe invalide"));
            }
        }

        /**
         * @brief Empile un élément
         * @param element
         */
        void pushstr(T element) {
            pile.push_back(element);
        }

        /**
         * @brief Depile un élément de la pile de string
         */
        void popstr() {
            depiler();
        }

        /**
         * @brief Affiche le dernier élément de la pile de string
         */
        void printstr() {
             console->append(QString::number(depiler()));
        }

        /**
         * @brief Copie un élément de la chaine de string en fonction du dernier élément de la pile numérique
         */
        void copystr() {
            copy(pilenum->depiler());
        }

        /**
         * @brief Affiche la pile de string
         */
        void displaystr() {
            console->append(QString("Pile String:" + showstr()));
        }

        /**
         * @brief Empile le nombre d'élément de la pile de string dans la pile numérique
         */
        void countstr() {
            pilenum->push(compter());
        }

        /**
         * @brief Inverse les deux derniers éléments de la pile de string
         */
        void swapstr() {
            swap();
        }

        /**
         * @brief Permet d'afficher la pile de string
         * @return QString du contenue de la pile de string
         */
        QString showstr() const {
            QString result;
            result = "[";
            for (unsigned int i=0; i< pile.size(); i++){
                result += QString(pile[i]) + " ";
            }
            result += "]";
            return result;
        }
		
		void drawstr() {
			QString texte;
			int x,y;
			
			y = pilenum->depiler();
			x = pilenum->depiler();
			texte = depiler();
			
			graphiques->addText(x, y, texte);			
		}

        /**
         * FIN commandes sur la pile numérique
         */
};
#endif

