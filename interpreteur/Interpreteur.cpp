#include "Interpreteur.h"

#include "PileDyn.hpp"

Interpreteur::Interpreteur(QTextEdit *_console, FenetreGraphique *_graphiques, int _largeur, int _hauteur) : console(_console), graphiques(_graphiques), largeur(_largeur), hauteur(_hauteur)
{}

//Lecture d'un fichier et exécution des commandes
void Interpreteur::lireFichier(QString chemin)
{
    QString choix;

    //Ouverture du fichier
    QFile fichier(chemin);
    fichier.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream flux(&fichier);

    //Vide la console
    console->clear();

    //Création des pile de double et de string
    Pile<double> piledouble(console, &flux, graphiques);
    Pile<QString> pilestr(console, &flux, graphiques, &piledouble);

    typedef void (Pile<double>::*myFunc)(void);
    typedef void (Pile<QString>::*myFuncstr)(void);

    typedef map<QString, myFunc> MyMap;
    typedef map<QString, myFuncstr> MyMapstr;

    MyMap myMap;
    MyMapstr myMapstr;

    //Association des mot-clés aux fonctions
    myMap[QString("pop")] = &Pile<double>::pop;
    myMap[QString("display")] = &Pile<double>::display;
    myMap[QString("push")] = &Pile<double>::push;
    myMap[QString("swap")] = &Pile<double>::swap;
    myMap[QString("copy")] = &Pile<double>::copy;
    myMap[QString("print")] = &Pile<double>::print;
    myMap[QString("signe")] = &Pile<double>::signe;
    myMap[QString("inverse")] = &Pile<double>::inverse;
    myMap[QString("+")] = &Pile<double>::plus;
    myMap[QString("-")] = &Pile<double>::minus;
    myMap[QString("count")] = &Pile<double>::count;
    myMap[QString("sin")] = &Pile<double>::sin;
    myMap[QString("cos")] = &Pile<double>::cos;
    myMap[QString("tan")] = &Pile<double>::tan;
    myMap[QString("sqrt")] = &Pile<double>::sqrt;
    myMap[QString("trunc")] = &Pile<double>::trunc;
    myMap[QString("line")] = &Pile<double>::line;
    myMap[QString("color")] = &Pile<double>::color;
    myMap[QString("#")] = &Pile<double>::comment;

    myMapstr[QString("pushstr")] = &Pile<QString>::pushstr;
    myMapstr[QString("popstr")] = &Pile<QString>::popstr;
    myMapstr[QString("copystr")] = &Pile<QString>::copystr;
    myMapstr[QString("displaystr")] = &Pile<QString>::displaystr;
    myMapstr[QString("countstr")] = &Pile<QString>::countstr;
    myMapstr[QString("swapstr")] = &Pile<QString>::swapstr;
    myMapstr[QString("drawstr")] = &Pile<QString>::drawstr;

    myFunc f;
    myFuncstr s;

    //Définition de la largeur de la fenêtre affichant du graphique
    int largeurFen = 500;
    int hauteurFen = 500;

    //Si des valeurs personnalisés sont entrées on les ajoutés
    if(largeur != 0)
        largeurFen = largeur;

    if(hauteur != 0)
        hauteurFen = hauteur;

    //Ajout de la largeur et de la hauteur dans la pile de doubles
    piledouble.push(largeurFen);
    piledouble.push(hauteurFen);

    //Lecture du fichier et exécution mot par mot
    while(! flux.atEnd()) {

        flux >> choix;

        if(myMap[choix])
        {
            f = myMap[choix];
            (piledouble.*f)();
        }
        else if(myMapstr[choix]) {
            s = myMapstr[choix];
            (pilestr.*s)();
        }
        else if(choix == "exit"){
            return;
        }
        else if(choix != ""){
            console->append("Commande non trouvée");
        }
    }
}
