Calculatrice Polonaise Inversée
==============================

Projet de création d'une calculatrice polonaise inversée

Membres du projet :

- Clément Lafont | [clem.lafont@gmail.com](mailto:clem.lafont@gmail.com)
- Alexandre Sanigou | [alexandre.sanigou@gmail.com](mailto:alexandre.sanigou@gmail.com)

TODO List
---------

- [x] Opérations de base
- [x] Remplacer else if
- [x] Lecture de fichiers NPI
- [x] Interface graphique
- [ ] Gestion des erreurs
- [x] Makefile
- [x] Doxygen
